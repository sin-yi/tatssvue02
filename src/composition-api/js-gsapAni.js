import{ onMounted  } from 'vue'
import { gsap } from 'gsap'


export function GsapAni() {
  const aniLan = 
    onMounted(() => {
      gsap.set(".js-titJp", {
        opacity: 0,
        height: 0,
      });
      gsap.set(".js-titKr", {
        opacity: 0,
        height: 0,
      });
      gsap.set(".js-titEn", {
        opacity: 1,
      });
      const tlcomic = gsap.timeline().repeat(-1);
      tlcomic.to(".js-titEn", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 2.5,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titJp", {
        opacity: 1,
        duration: 2.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titJp", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titKr", {
        opacity: 1,
        duration: 2.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titKr", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
  });
  const aniDecBt = 
    onMounted(() => {
      gsap.to(".js-decBt", {
        boxShadow: "0 0 0 12px rgba(255,215,0,0.24)" ,
        duration: 2,
        repeat: -1,
    });
  });
  return{
      aniDecBt,
      aniLan
  }
}

export function LanAni() {
  const aniLan = 
    onMounted(() => {
      gsap.set(".js-titJp", {
        opacity: 0,
        height: 0,
      });
      gsap.set(".js-titKr", {
        opacity: 0,
        height: 0,
      });
      gsap.set(".js-titEn", {
        opacity: 1,
      });
      const tlcomic = gsap.timeline().repeat(-1);
      tlcomic.to(".js-titEn", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 2.5,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titJp", {
        opacity: 1,
        duration: 2.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titJp", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titKr", {
        opacity: 1,
        duration: 2.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
      tlcomic.to(".js-titKr", {
        opacity: 0,
        height: 0,
        duration: 0.5,
        delay: 0,
        ease: "Power3.easeInOut",
      });
  });
  return{
      aniLan
  }
}
