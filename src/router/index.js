
import Vue from "vue";
import { createRouter, createWebHistory, createWebHashHistory, beforeEach, query, route } from 'vue-router'
import Login from '../views/Login.vue'
import Index from '../views/Index.vue'


const routes = [
  {
    path: "/",
    name: "index",
    component: Index,
    // props: true,
    // 網址傳參數需要在此傳遞下去
    //https://stackoverflow.com/questions/66252128/how-to-get-query-params-in-vue-js-3-setup
    props: route => ({ query: route.query }),

  },
  {
    path: '/main',
    name: 'Main',
    props: route => ({ query: route.query }),
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Main.vue'),
  },
  {
    path: '/reason',
    name: 'Reason',
    props: route => ({ query: route.query }),
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Reason.vue'),
  },
  {
    path: '/resultGood',
    name: 'ResultGood',
    props: route => ({ query: route.query }),
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ResultGood.vue'),
  },
  {
    path: '/resultBad',
    name: 'ResultBad',
    props: route => ({ query: route.query }),
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ResultBad.vue'),
  }
]

const router = createRouter({
  // createWebHashHistory前面會產生路徑會產生#/
  history: createWebHistory(process.env.BASE_URL),
  mode: 'history',
  routes
})

// baseURL是你API的主要Domain，之後發請求時只要填相對路徑就可以了
// const instance = axios.create({
//   baseURL: 'http://192.168.1.3:8080/',
//   headers: { 'Content-Type': 'application/json' },
//   timeout: 20000,
//   proxy: {
//       // host: '127.0.0.1',
//       // port: 9000,
//       auth: {
//       adminGroup: '202110000001',
//       }
//   },
// });



// router.beforeEach((to, from, next) => {
//   if (!to.query.adminGroup) {
//     next({
//       path: to.path,
//       // query: {
//       //   adminGroup: '123'
//       // }
//     });
//   } else {
//     next();
//   }
//   routes
// });

export default router
