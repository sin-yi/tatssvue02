import { createApp, h ,render, Vue } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import gsap from 'gsap'
//使用vue meta 3.0.0-alpha.7
import { createMetaManager } from 'vue-meta'
import axios from 'axios'
import VueAxios from 'vue-axios'
// axios.defaults.baseURL='http://192.168.1.3:8080/'
// axios.defaults.headers.post['Content-Type'] = 'application/json'


// 放在main.js，在css載入順序中會被最後載入
// import '@/assets/css/vendors/cssreset.css'
// import '@/assets/css/vendors/unreset.css'
// import '@/assets/css/layout.css'
import '@/assets/css/app.sass'
//manifest
/* <link rel="manifest" href="js/manifest.json"></link> */
import '@/composition-api/manifest.json'




const app = createApp(App)
  .use(router)
  .use(store)
  .use(gsap)
  .use(VueAxios, axios)
  .use(createMetaManager()) 

app.mount('#app')

    