import{ onMounted  } from 'vue'
import { gsap } from 'gsap'


export function GsapAni() {
    const aniDecBt = 
        onMounted(() => {
            gsap.to(".js-decBt", {
                boxShadow: "0 0 0 12px rgba(255,215,0,0.24)" ,
                duration: 2,
                repeat: -1,
            });
        });
    return{
        aniDecBt
    }
}
