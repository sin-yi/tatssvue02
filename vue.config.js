const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;
module.exports = {
  transpileDependencies: ['vue-meta'],
  lintOnSave: false
}

module.exports = {
  //不希望打包檔案出現雜湊值 filenameHashing: false
  filenameHashing: false, 
  //如果~@路徑不起作用時設置
  //@与~都是 路径别名，方便我们引用文件。
  //@主要使用在js与html内，~则是在@之前添加~,即~@,主要用于css引用和html内。
  chainWebpack: config => {
      config.resolve.alias.set('@', path.join(__dirname, './src'));
  },
  // 开启 CSS source maps
  css: {
    sourceMap: true, 
  },
  //PrerenderSPA Plugin 設定
  configureWebpack: config => {
    return{
      plugins: [
        new PrerenderSPAPlugin({
          // Required - The path to the webpack-outputted app to prerender.
          staticDir: path.join(__dirname, 'dist'),//輸出的路徑，設定在原本webpack輸出的dist資料夾即可
          // Required - Routes to render.
          routes: [ '/index', '/main', '/reason', '/resultGood', '/resultBad'  ],//要渲染的頁面
          // Available renderers: https://github.com/Tribex/prerenderer/tree/master/renderers
          //renderer選填設定
          //renderer: new Renderer({
            // vue3的mount我找不到如何掛載的設定，所以先不要用...
            //renderAfterDocumentEvent: 'render-event',//這一段的參數一定要對應main.js中的設定，
            // Other puppeteer options.
            // (See here: https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#puppeteerlaunchoptions)
            //headless: true, // Display the browser window when rendering. Useful for debugging.
          //})
        })
      ]
    }
  }
}


//開發中先建立一個暫時的代理伺服器，把遠端api的檔案路徑，
//先指向localhost的/api資料夾
// module.exports = {
//     devServer:{
//         proxy:{
//             '/api': {
//                 //target為遠端網域路徑
//                 target: 'http://192.168.1.3:8080/',
//                 //指向資料夾
//                 pathRewrite:{ '^/api': ''},
//                 changeOrigin: true,
//                 ws: true          
//             },
//         }
//     }   
// }

